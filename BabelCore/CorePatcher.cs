﻿using BepInEx;
using BepInEx.Logging;
using BepInEx.Preloader.Core.Patching;
using Mono.Cecil;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace BabelCore;

[PatcherPluginInfo("org.jokleinn.babelcore", "Babel Core", "1.0")]
public class EP : BasePatcher
{

    private void process_exited(object sender, EventArgs ea) => EP.Log.LogInfo("[Updater] Done.");
    private void process_stderr(object sender, DataReceivedEventArgs ea)
    {
        if (!ea.Data.IsNullOrWhiteSpace())
            EP.Log.LogWarning("[Updater] " + ea.Data);
    }
    private void process_stdout(object sender, DataReceivedEventArgs ea)
    {
        if (!ea.Data.IsNullOrWhiteSpace())
            EP.Log.LogInfo("[Updater] " + ea.Data);
    }

    private void update(string version, string babeldllpath)
    {
        Process p = new Process();
        p.Exited += new EventHandler(process_exited);
        p.ErrorDataReceived += new DataReceivedEventHandler(process_stderr);
        p.OutputDataReceived += new DataReceivedEventHandler(process_stdout);

        p.StartInfo.FileName = Path.Combine(new DirectoryInfo(Path.GetDirectoryName(babeldllpath)).Parent.FullName, "updater", "BabelUpdater.exe");
        p.StartInfo.Arguments = "\"" + version.ToString() + "\" \"" + babeldllpath + "\"";
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.RedirectStandardOutput = true;

        p.Start();
        p.BeginErrorReadLine();
        p.BeginOutputReadLine();

        p.WaitForExit();
    }

    public override void Initialize()
    {
        EP.Log = base.Log;

        Log.LogInfo("Babel Core initializing...");
        Log.LogInfo("Checking Babel plugin version...");
        FileInfo babelcoredll = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        string babeldllpath   = Path.Combine(babelcoredll.Directory.FullName, "plugins", "Babel.dll");
        Version installed_version = null;
        if (File.Exists(babeldllpath))
        {
            //...load babel dll
            AppDomain checkdomain = AppDomain.CreateDomain("babel_version_check");
            AssemblyName aname = new AssemblyName();
            aname.CodeBase = babeldllpath;
            Assembly assy = checkdomain.Load(aname);
            installed_version = (Version)assy.GetName().Version.Clone();
            AppDomain.Unload(checkdomain);
        }
        else
            return;

        if (installed_version != null)
            Log.LogInfo("Found Babel version " + installed_version.ToString());
        else
            return;

        update(installed_version.ToString(), babeldllpath);
    }

    internal static new ManualLogSource Log;

    public override void Finalizer() => Log.LogInfo("Babel Core initialized!");

    [TargetType("Assembly-CSharp.dll", "AssemblyManager")]
    public void Patch(TypeDefinition t)
    {
        Log.LogInfo("Patching " + t.Name + "'s nested types...");
        var types = t.NestedTypes;
        foreach (var type in types)
        {
            if (type.FullName.Contains("TypeDictionary"))
            {
                type.IsPublic = true;
                type.IsNotPublic = false;
            }
        }
    }
}
