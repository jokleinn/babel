# Babel

Babel is a drop-in replacement mod loader for Library of Ruina. It uses BepInEx 6 to overwrite the base game's mod loader.

It allows workshop mods to specify dependencies and incompatibilities with each other, and it sorts the list of .dll's that it has to load in order to ensure that it respects these dependencies.

Babel can autoupdate itself from this GitLab repository.

To install, install BepInEx 6, then copy the contents of this repository's "prepack" directory into the BepInEx folder.

You can download the prepack by grabbing the release artifact. See the image below.

![Release how-to](https://i.imgur.com/xiFhQAl.png)

## For Modders

Link your assembly against BabelInterop.dll. You can then provide as many BabelInterop assembly traits as needed in order to define compatibility with other mods.

    using Babel.Interop;
    
    [assembly:BabelInterop("Kael'thas mod library", interop_type.REQUIRED)] //we need Kael'thas mod library to function.
    [assembly:BabelInterop("Kel'Thuzad extensions", interop_type.SOFT)] //if Kel'Thuzad extensions is present, we can use it, so we prefer that it loads before us.

    [assembly:BabelInterop("Annoying assembly", interop_type.INCOMPATIBLE)] //Annoying assembly just doesn't work with our mod.

    [assembly:BabelInterop("Buggy mod", interop_type.INCOMPATIBLE, "1.1.0", version_comparator.EXACT)] Buggy mod 1.1.0 breaks our mod. All other versions of it are fine.

    [assembly:BabelInterop("Kali Untamed MOD", interop_type.SOFT, "1.1.2")] //prefer to load before all versions of Kali Untamed MOD at or above 1.1.2. ignore versions below this.

    [assembly:BabelInterop("Recently fixed mod", interop_type.INCOMPATIBLE, "1.4", version_comparator.AT_MOST)] //version 1.5 of Recently fixed mod is now compatible. So, we say that all versions up to version 1.4 are incompatible.

You do *not* need to link against Babel.dll. You only need to link against BabelInterop.dll.

If you want your workshop mod to be able to load when Babel is not installed (I recommend this, so that we don't annoy people!), provide BabelInterop.dll alongside your mod's .dll in the Assemblies folder. If Babel is not installed on an end user's computer and you do not provide BabelInterop.dll with your mod, they will not be able to load your mod.

## Future Plans / TODO

- Core system for mods to use, to ensure that mods do not have to patch the game themselves; should increase compatibility between mods

- BaseMod/BBE loading support. Once the core framework described above is implemented, we can implement shims to make it possible to load these older/alternative mods as well, so that people only need to have one ModLoader on their system and can have full dependency checking and compatibility assurance for all of their mods.



