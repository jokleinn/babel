# Contributing

You are **NOT** permitted to write anything to the "prepack" directory.
Any merge request that does this will be immediately rejected for safety reasons.
If you wish to verify that the .dll's in the prepack directory are not malicious, feel free to open the managed dll's in dnSpy.
The libgit2 dll comes from LibGit2Sharp. It is not managed code, so dnSpy may not show anything understandable. If you wish, you can build libgit2 for yourself, and replace that dll with your own built version. Alternatively, you may verify the signature of the libgit2 dll against the dll that LibGit2Sharp provides by opening the .nupkg for LibGit2Sharp. I apologize for the trust I am asking of you.

Code should be understandable and clean. I am willing to work with anybody who has questions, hopefully we will be able to make sure Babel suits everybody's needs.
