﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using Mod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

//TODO: MAJOR: Workshop mod with BabelIgnore that downloads BepInEx and BabelCore, then exits the game

namespace Babel.Interop;

//Remember: version bumps also must be done with the assembly version info
[BepInPlugin("org.jokleinn.babel", "Babel", "1.3.1")]
[BepInProcess("LibraryOfRuina.exe")]
public class Plugin : BaseUnityPlugin
{
    private void Awake()
    {
        // Plugin startup logic
        Plugin.Log = base.Logger;

        //for AssemblyManager
        initializerList = new List<BabelInitializer>();

        Log.LogInfo("Babel is patching the mod management classes...");
        Harmony.CreateAndPatchAll(Assembly.GetExecutingAssembly());
        Log.LogInfo("Done.");
    }

    internal static ManualLogSource Log;

    internal static void register_initializer(Assembly assy, ModInitializer mi, Version vers) => initializerList.Add(new BabelInitializer(assy, mi, vers));

    internal static bool precheck_mods()
    {
        Plugin.Log.LogInfo("Babel is checking the mod list for missing versions or incompatibilities...");
        bool ret = true;
        foreach (BabelInitializer init in initializerList)
        {
            List<BabelInterop> incompatible, missing;
            if (!init.precheck(initializerList, out incompatible, out missing))
            {
                var mcm = Singleton<ModContentManager>.Instance;
                foreach (var incomp in incompatible)
                {
                    string err = init.name + " " + incomp.description;
                    if (incomp.version != null)
                    {
                        var found = initializerList.Where(bi => bi.name == incomp.depname);
                        foreach (var f in found)
                            mcm.AddErrorLog(err + "... found " + f.description + ", which matches the incompatibility");
                    }
                    else
                        mcm.AddErrorLog(err + ", and you are trying to load it!");
                }
                foreach(var miss in missing)
                {
                    string err = init.name + " " + miss.description;
                    var found = initializerList.Where(bi => bi.name == miss.depname);
                    foreach (var f in found)
                        mcm.AddErrorLog(err + "... found " + f.description + ", which is insufficient!");
                    if (!found.Any())
                        mcm.AddErrorLog(err + ", and it is not in the mod list!");
                }
                ret = false;
            }
        }
        return ret;
    }


    private static void rebuild_and_retry_initialization()
    {
        //TODO: it works as-is, but we could investigate finding all dependent mods that
        //are no longer happy to load, and dropping them here. it could lead to prettier
        //error logging, but no change in functionality.
        Log.LogWarning("Rebuilding and validating mod list...");
        initializerList.RemoveAll(bi => bi.status == BabelInitializer.status_type.failed);
        sort_mods(true);
        initialize_mods();
    }

    internal static void initialize_mods()
    {
        bool retry = false;
        List<BabelInitializer> loadable = new List<BabelInitializer>(initializerList.Where(bi => bi.status == BabelInitializer.status_type.uninitialized));
        foreach (BabelInitializer init in loadable)
        {
            try
            {
                Log.LogInfo("Initializing " + init.description + "...");
                init.initialize();
            }
            catch (Exception e)
            {
                var inst = Singleton<ModContentManager>.Instance;
                inst.AddErrorLog(init.description + " failed to initialize:");
                inst.AddErrorLog(e);
                retry = true;
                break;
            }
        }
        if (retry)
            rebuild_and_retry_initialization();

        List<Assembly> l = new List<Assembly>();
        foreach (BabelInitializer BI in initializerList)
        {
            l.Add(BI.assy);
        }
        Singleton<BabelCallbacks>.Instance.iv_OnInitialization(l);
    }

    /// <summary>
    /// Kahn's Algorithm.
    /// </summary>
    internal static void sort_mods(bool rebuilt = false)
    {
        if (initializerList.Count == 0)
            return;

        Log.LogInfo("Babel is dep-sorting " + initializerList.Count + " mods that have initializers...");

        //Empty list for output
        List<BabelInitializer> L = new List<BabelInitializer>();

        //Build S, the set of all nodes that don't depend on anything
        List<BabelInitializer> S = new List<BabelInitializer>(
            initializerList.Where(bi => !bi.dependent)
        );
        initializerList.RemoveAll(bi => !bi.dependent);

        if (S.Count == 0)
        {
            //everything in the graph has at least a soft dependency
            //ignore soft dependencies, temporarily
            S.AddRange(initializerList.Where(bi => !bi.dependent_hard));
            initializerList.RemoveAll(bi => !bi.dependent_hard);
        }

        if (S.Count == 0)
        {
            //everything in the graph has a hard dependency on something else
            throw new Exception("The dependency graph has no root nodes. Every mod in your mods list has a hard dependency on another mod.");
        }

        DateTime start = DateTime.Now;
        TimeSpan timeout = TimeSpan.FromSeconds(30);
        while (S.Count > 0)
        {
            System.Threading.Thread.Sleep(10);
            if (start - DateTime.Now > timeout)
            {
                var mcm = Singleton<ModContentManager>.Instance;
                mcm.AddErrorLog("");
                mcm.AddErrorLog("Dependency sorting has taken 30 seconds.");
                mcm.AddErrorLog("Cancelling dependency sorting. It is possible,");
                mcm.AddErrorLog(" although unlikely, that this is just a ");
                mcm.AddErrorLog(" hardware performance issue.");
                mcm.AddErrorLog("");
                break;
            }
            BabelInitializer n = S[0];
            S.Remove(n);
            L.Add(n);
            while (true)
            {
                int len_before = initializerList.Count;
                bool full = false;
                foreach (BabelInitializer init in initializerList)
                {
                    List<BabelInterop> soft_missing;
                    BabelInitializer.satisfaction_type sat = init.is_satisfied(L, out soft_missing);
                    if (sat == BabelInitializer.satisfaction_type.satisfied_fully)
                    {
                        S.Add(init);
                        initializerList.Remove(init);
                        full = true;
                        break;
                    }
                }
                if (!full) //check again, but this time ignore soft dependencies
                    foreach (BabelInitializer init in initializerList)
                    {
                        List<BabelInterop> soft_missing;
                        BabelInitializer.satisfaction_type sat = init.is_satisfied(L, out soft_missing);
                        if (sat is BabelInitializer.satisfaction_type.satisfied_fully or
                            BabelInitializer.satisfaction_type.satisfied_requirements)
                        {
                            S.Add(init);
                            initializerList.Remove(init);
                            break;
                        }
                    }

                //if we go through everything and can't find a single mod to add to S,
                //we are stuck. time to bail.
                if (len_before == initializerList.Count)
                    break;
            }
        }

        if (initializerList.Any())
        {
            var mcm = Singleton<ModContentManager>.Instance;
            mcm.AddErrorLog("");
            mcm.AddErrorLog("Dependency sorting did not resolve all dependencies.");
            mcm.AddErrorLog("");
            if (rebuilt)
            {
                mcm.AddErrorLog("The mod list was rebuilt.");
                mcm.AddErrorLog("A mod that failed to load would cause issues");
                mcm.AddErrorLog(" for every mod that depended on it.");
            }
            else
            {
                mcm.AddErrorLog("It is likely that you have cyclic dependencies");
                mcm.AddErrorLog(" in your mod list, e.g. mod A wants mod B and mod B wants mod A.");
            }
            mcm.AddErrorLog("");
            mcm.AddErrorLog("Dumping remaining list...");
            foreach (var t in initializerList)
            {
                mcm.AddErrorLog(t.description);
                t.describe_dependencies();
            }
            mcm.AddErrorLog("");
            mcm.AddErrorLog("Check the console for a detailed description of dependencies.");
            mcm.AddErrorLog("You may wish to disable these mods or contact their developers.");
        }

        initializerList = new List<BabelInitializer>(L);
    }

    private static List<BabelInitializer> initializerList;
}
