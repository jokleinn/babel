﻿using Babel.Interop;
using GameSave;
using HarmonyLib;
using LOR_XML;
using Mod;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Babel;

internal static class constants
{
    /// <summary>
    /// If a mod ever puts this in its id, we'll run into trouble.
    /// The constant is kept here so that it will be easy to find and change later if that happens.
    /// </summary>
    internal static string string_separator = "|||";
}

// ---------------------------------------------------------
// MODCONTENT
// ---------------------------------------------------------

[HarmonyPatch(typeof(ModContent), "LoadAssemblies")]
class Patch_ModContent_LoadAssemblies
{
    static bool Prefix(ModContentInfo ____modInfo, DirectoryInfo ____dirInfo)
    {
        List<string> fnames = new List<string>();
        DirectoryInfo di = new DirectoryInfo(Path.Combine(____dirInfo.FullName, "Assemblies"));
        if (di.Exists)
            foreach (FileInfo fi in di.GetFiles())
                if (fi.Extension.ToLower().Equals(".dll"))
                    fnames.Add(fi.FullName);

        string uid = ____modInfo.invInfo.workshopInfo.uniqueId;
        string modver = ____modInfo.modVer.ToString();
        string passthrough = uid + constants.string_separator + modver;
        Dictionary<string, BattleCardAbilityDesc> dict;
        Singleton<AssemblyManager>.Instance.LoadAllAssembly(passthrough, fnames.ToArray(), out dict);
        Singleton<BattleCardAbilityDescXmlList>.Instance.AddByMode(uid, dict);
        return false;
    }
}

// ---------------------------------------------------------
// ASSEMBLYMANAGER
// ---------------------------------------------------------

class AssemblyManager_Helpers : AssemblyManager
{
    public static void LoadTypesFromAssembly(Assembly assy, string version,
        ref AssemblyManager.TypeDictionary<DiceCardSelfAbilityBase> ____diceCardSelfAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardAbilityBase> ____diceCardAbilityDict,
        ref AssemblyManager.TypeDictionary<BehaviourActionBase> ____behaviourActionDict,
        ref AssemblyManager.TypeDictionary<PassiveAbilityBase> ____passiveAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardPriorityBase> ____diceCardPriorityDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitAggroSetter> ____enemyUnitAggroSetterDict,
        ref AssemblyManager.TypeDictionary<EnemyTeamStageManager> ____enemyTeamStageManagerDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitTargetSetter> ____enemyUnitTargetSetterDict
    )
    {
        if (assy.GetCustomAttributes(typeof(BabelIgnore)).Any())
            return;

        foreach (Type t in assy.GetTypes())
        {
            string tn = t.Name;
            if (t.IsSubclassOf(typeof(DiceCardSelfAbilityBase)) && tn.StartsWith("DiceCardSelfAbility_"))
            {
                ____diceCardSelfAbilityDict.Add(tn.Substring("DiceCardSelfAbility_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(DiceCardAbilityBase)) && tn.StartsWith("DiceCardAbility_"))
            {
                ____diceCardAbilityDict.Add(tn.Substring("DiceCardAbility_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(BehaviourActionBase)) && tn.StartsWith("BehaviourAction_"))
            {
                ____behaviourActionDict.Add(tn.Substring("BehaviourAction_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(PassiveAbilityBase)) && tn.StartsWith("PassiveAbility_"))
            {
                ____passiveAbilityDict.Add(tn.Substring("PassiveAbility_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(DiceCardPriorityBase)) && tn.StartsWith("DiceCardPriority_"))
            {
                ____diceCardPriorityDict.Add(tn.Substring("DiceCardPriority_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(EnemyUnitAggroSetter)) && tn.StartsWith("EnemyUnitAggroSetter_"))
            {
                ____enemyUnitAggroSetterDict.Add(tn.Substring("EnemyUnitAggroSetter_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(EnemyTeamStageManager)) && tn.StartsWith("EnemyTeamStageManager_"))
            {
                ____enemyTeamStageManagerDict.Add(tn.Substring("EnemyTeamStageManager_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(EnemyUnitTargetSetter)) && tn.StartsWith("EnemyUnitTargetSetter_"))
            {
                ____enemyUnitTargetSetterDict.Add(tn.Substring("EnemyUnitTargetSetter_".Length), t);
            }
            else if (t.IsSubclassOf(typeof(ModInitializer)))
            {
                Plugin.register_initializer(
                    assy,
                    Activator.CreateInstance(t) as ModInitializer,
                    new Version(version)
                );
            }
        }
    }
}

[HarmonyPatch(typeof(AssemblyManager), nameof(AssemblyManager.CallAllInitializer))]
class Patch_AssemblyManager_CallAllInitializer
{
    static bool Prefix()
    {
        var inst = Singleton<ModContentManager>.Instance;
        try
        {
            if (!Plugin.precheck_mods())
            {
                //precheck function expected to explain its problem
                inst.AddErrorLog("Mod code will not be loaded.");
                return false;
            }
            Plugin.sort_mods();
        }
        catch (Exception e)
        {
            inst.AddErrorLog(e);
            inst.AddErrorLog("Mod code will not be loaded.");
            return false;
        }
        //has its own try-catch
        Plugin.initialize_mods();
        return false;
    }
}

[HarmonyPatch(typeof(AssemblyManager), nameof(AssemblyManager.LoadAllAssembly))]
class Patch_AssemblyManager_LoadAllAssembly
{
    static bool Prefix(ref AssemblyManager __instance, string uid, string[] filenames, out Dictionary<string, BattleCardAbilityDesc> abilityText,
        ref AssemblyManager.TypeDictionary<DiceCardSelfAbilityBase> ____diceCardSelfAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardAbilityBase> ____diceCardAbilityDict,
        ref AssemblyManager.TypeDictionary<BehaviourActionBase> ____behaviourActionDict,
        ref AssemblyManager.TypeDictionary<PassiveAbilityBase> ____passiveAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardPriorityBase> ____diceCardPriorityDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitAggroSetter> ____enemyUnitAggroSetterDict,
        ref AssemblyManager.TypeDictionary<EnemyTeamStageManager> ____enemyTeamStageManagerDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitTargetSetter> ____enemyUnitTargetSetterDict,
        ref Dictionary<string, List<Assembly>> ____assemblyDict
    )
    {
        string[] args = uid.Split(new string[] {constants.string_separator}, StringSplitOptions.RemoveEmptyEntries);
        string realuid = args[0];
        string modversion = args[1];

        abilityText = new Dictionary<string, BattleCardAbilityDesc>();

        if (____assemblyDict.ContainsKey(realuid))
            return false;

        List<Assembly> assys = new List<Assembly>();
        foreach (string fn in filenames)
        {
            try
            {
                List<Assembly> curr_assys = new List<Assembly>(AppDomain.CurrentDomain.GetAssemblies());
                Assembly assy = Assembly.LoadFile(fn);
                if (assy != null)
                {
                    Plugin.Log.LogDebug("Reading assembly: " + fn);
                    if (curr_assys.Contains(assy))
                    {
                        Plugin.Log.LogDebug("IGNORED: The same assembly name already exists: " + assy.GetName().Name);
                    }
                    else
                    {
                        //CustomInvitation::AssemblyDataManager::ReloadDll does this too
                        foreach (Type t in assy.GetTypes())
                        {
                            string tn = t.Name;
                            bool diceAbility = false;
                            if (t.IsSubclassOf(typeof(DiceCardAbilityBase)))
                            {
                                if (tn.StartsWith("DiceCardAbility_"))
                                    tn = tn.Substring("DiceCardAbility_".Length);
                                diceAbility = true;
                            }
                            if (t.IsSubclassOf(typeof(DiceCardSelfAbilityBase)))
                            {
                                if (tn.StartsWith("DiceCardSelfAbility_"))
                                    tn = tn.Substring("DiceCardSelfAbility_".Length);
                                diceAbility = true;
                            }

                            if (diceAbility)
                            {
                                FieldInfo fi = t.GetField("Desc");
                                if (fi != null)
                                {
                                    string desc = fi.GetValue(null) as string;
                                    if (!string.IsNullOrEmpty(desc) && !abilityText.ContainsKey(tn))
                                    {
                                        abilityText.Add(tn, new BattleCardAbilityDesc
                                        {
                                            id = tn,
                                            desc = new List<string>
                                            {
                                                desc
                                            }
                                        });
                                    }
                                }
                            }
                        }
                        assys.Add(assy);
                    }
                }
                else
                {
                    Singleton<ModContentManager>.Instance.AddErrorLog("Loading failed: " + fn);
                }
            }
            catch (Exception e)
            {
                Singleton<ModContentManager>.Instance.AddErrorLog(e);
            }
        }
        foreach (Assembly assy in assys)
        {
            AssemblyManager_Helpers.LoadTypesFromAssembly(assy, modversion,
                ref ____diceCardSelfAbilityDict,
                ref ____diceCardAbilityDict,
                ref ____behaviourActionDict,
                ref ____passiveAbilityDict,
                ref ____diceCardPriorityDict,
                ref ____enemyUnitAggroSetterDict,
                ref ____enemyTeamStageManagerDict,
                ref ____enemyUnitTargetSetterDict
            );
        }

        if (assys.Count > 0)
        {
            ____assemblyDict.Add(realuid, assys);
        }

        return false;
    }
}

[HarmonyPatch(typeof(AssemblyManager), nameof(AssemblyManager.Initialize))]
class Patch_AssemblyManager_Initialize
{
    static bool Prefix(ref AssemblyManager __instance,
        ref AssemblyManager.TypeDictionary<DiceCardSelfAbilityBase> ____diceCardSelfAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardAbilityBase> ____diceCardAbilityDict,
        ref AssemblyManager.TypeDictionary<BehaviourActionBase> ____behaviourActionDict,
        ref AssemblyManager.TypeDictionary<PassiveAbilityBase> ____passiveAbilityDict,
        ref AssemblyManager.TypeDictionary<DiceCardPriorityBase> ____diceCardPriorityDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitAggroSetter> ____enemyUnitAggroSetterDict,
        ref AssemblyManager.TypeDictionary<EnemyTeamStageManager> ____enemyTeamStageManagerDict,
        ref AssemblyManager.TypeDictionary<EnemyUnitTargetSetter> ____enemyUnitTargetSetterDict
    )
    {
        //Vanilla has "GetExecutingAssembly" here but we actually want to get Assembly-CSharp.dll
        //We assume AssemblyManager lives in Assembly-CSharp.dll. We could use any base class
        //if we wanted, too, but I doubt any mods will wholly replace AssemblyManager in memory.
        Plugin.Log.LogInfo("Loading base game content using Babel...");
        Assembly ex = Assembly.GetAssembly(typeof(AssemblyManager));
        AssemblyManager_Helpers.LoadTypesFromAssembly(ex, ex.GetName().Version.ToString(),
            ref ____diceCardSelfAbilityDict,
            ref ____diceCardAbilityDict,
            ref ____behaviourActionDict,
            ref ____passiveAbilityDict,
            ref ____diceCardPriorityDict,
            ref ____enemyUnitAggroSetterDict,
            ref ____enemyTeamStageManagerDict,
            ref ____enemyUnitTargetSetterDict
        );
        return false;
    }
}

// ---------------------------------------------------------
// MODCONTENTMANAGER
// ---------------------------------------------------------

[HarmonyPatch(typeof(ModContentManager), nameof(ModContentManager.SetActiveContents))]
class Patch_ModContentManager_SetActiveContents
{
    static bool Prefix(List<ModContentInfo> targets, ref ModContentManager __instance, ref List<ModContentInfo> ____allMods, ref List<ModContent> ____loadedContents)
    {
        foreach (ModContentInfo mci in ____allMods)
            mci.activated = false;

        List<ModContentInfo> list = new List<ModContentInfo>();
        //TODO: Parallelize this if possible.
        //May require patching ModContent::LoadModContent to make it threadsafe.
        //Which means we will probably have to patch Singleton. 
        using (List<ModContentInfo>.Enumerator en = targets.GetEnumerator())
        {
            while (en.MoveNext())
            {
                ModContentInfo mod = en.Current;
                if (list.Exists((ModContentInfo x) => x.invInfo.workshopInfo.uniqueId == mod.invInfo.workshopInfo.uniqueId))
                {
                    __instance.AddErrorLog("The same Mod already exists. ID " + mod.invInfo.workshopInfo.uniqueId + " was ignored.");
                }
                else
                {
                    Plugin.Log.LogInfo("Loading mod content for " + mod.invInfo.workshopInfo.title + "...");
                    ModContent mc = ModContent.LoadModContent(mod);
                    if (mc != null)
                    {
                        ____loadedContents.Add(mc);
                        list.Add(mod);
                    }
                }
            }
        }

        Singleton<AssemblyManager>.Instance.CallAllInitializer();
        __instance.SaveSelectionData(____allMods, list);

        return false;
    }
}

[HarmonyPatch(typeof(ModContentManager), nameof(ModContentManager.AddErrorLog), new Type[] { typeof(string) })]
class Patch_ModContentManager_AddErrorLog_string
{
    static bool Prefix(string msg, ref List<string> ____logs)
    {
        Plugin.Log.LogError(msg);
        ____logs.Add(msg);
        return false;
    }
}

[HarmonyPatch(typeof(ModContentManager), nameof(ModContentManager.AddErrorLog), new Type[] { typeof(string), typeof(Exception) })]
class Patch_ModContentManager_AddErrorLog_string_exception
{
    static bool Prefix(string msg, Exception e, ref List<string> ____logs)
    {
        Plugin.Log.LogError(e);
        ____logs.Add(e.Message);
        return false;
    }
}

[HarmonyPatch(typeof(ModContentManager), nameof(ModContentManager.AddErrorLog), new Type[] { typeof(Exception) })]
class Patch_ModContentManager_AddErrorLog_exception
{
    static bool Prefix(Exception e, ref List<string> ____logs)
    {
        Plugin.Log.LogError(e);
        ____logs.Add(e.Message);
        return false;
    }
}

[HarmonyPatch(typeof(ModContentManager), nameof(ModContentManager.LoadSelectionData))]
class Patch_ModContentManager_LoadSelectionData
{
    static string savePath => Path.Combine(SaveManager.savePath, "ModSetting.save");
    static bool Prefix(ref List<ModContentInfo> ____allMods)
    {
        List<string> orders = new List<string>();
        List<string> lastActivated = new List<string>();

        SaveData sd = Singleton<SaveManager>.Instance.LoadData(savePath);
        if (sd == null)
            return false;

        SaveData sd_orders = sd.GetData("orders");
        if (sd_orders != null)
        {
            foreach (SaveData sd_order in sd_orders)
            {
                string s = sd_order.GetStringSelf();
                if (!string.IsNullOrEmpty(s))
                {
                    orders.Add(s);
                }
            }
        }

        SaveData sd_lastActivated = sd.GetData("lastActivated");
        if (sd_lastActivated != null)
        {
            foreach (SaveData sd_last in sd_lastActivated)
            {
                string s = sd_last.GetStringSelf();
                if (!string.IsNullOrEmpty(s))
                {
                    lastActivated.Add(s);
                }
            }
        }

        List<ModContentInfo> mcis = new List<ModContentInfo>(____allMods);
        ____allMods.Clear();

        using (List<string>.Enumerator en = orders.GetEnumerator())
        {
            while (en.MoveNext())
            {
                string id = en.Current;
                ModContentInfo mci = mcis.Find((ModContentInfo x) => x.invInfo.workshopInfo.uniqueId == id);
                if (mci != null)
                {
                    mcis.Remove(mci);
                    ____allMods.Add(mci);
                }
            }
        }

        ____allMods.AddRange(mcis);

        using (List<string>.Enumerator en = lastActivated.GetEnumerator())
        {
            while (en.MoveNext())
            {
                string id = en.Current;
                Plugin.Log.LogInfo("Activating " + id);
                ModContentInfo mci = ____allMods.Find((ModContentInfo x) => x.invInfo.workshopInfo.uniqueId == id);
                if (mci != null)
                    mci.activated = true;
            }
        }

        return false;
    }
}
