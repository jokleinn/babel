﻿using Babel.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Babel;
internal class BabelInitializer
{
    public BabelInitializer(Assembly a, ModInitializer mi, Version v)
    {
        this.assy = a;
        this.init = mi;

        Version avers = a.GetName().Version;
        this.vers = avers.Equals(defvers) ? v : avers;

        List<BabelInterop> interops = new List<BabelInterop>();
        foreach (object attr in this.assy.GetCustomAttributes(typeof(BabelInterop)))
            interops.Add((BabelInterop)attr);
        this.interopslist = interops;
    }

    public Version version => this.vers;

    public string name => this.assy.GetName().Name;

    public string description => this.name + " " + this.vers.ToString();

    public bool dependent => this.interopslist.Any(bi =>
                                          bi.op is interop_type.REQUIRED or interop_type.SOFT
            );

    public bool dependent_hard => this.interopslist.Any(bi => bi.op == interop_type.REQUIRED);

    public List<BabelInterop> interops => this.interopslist;

    public void initialize()
    {
        try
        {
            this.init.OnInitializeMod();
        }
        catch (Exception)
        {
            this.init_status = status_type.failed;
            throw; //rethrow for initialize_mods to swallow it
        }
        this.init_status = status_type.initialized;
    }

    public enum status_type
    {
        uninitialized,
        initialized,
        failed
    };

    public status_type status => this.init_status;

    public void describe_dependencies()
    {
        Plugin.Log.LogInfo(this.description + ": ");
        foreach (BabelInterop bi in this.interopslist)
            Plugin.Log.LogInfo("..." + bi.description);
    }

    public bool precheck(
        IEnumerable<BabelInitializer> candidates,
        out List<BabelInterop> incompatible,
        out List<BabelInterop> missing
    )
    {
        incompatible = new List<BabelInterop>();
        missing = new List<BabelInterop>(
            this.interopslist.Where(op => op.op == interop_type.REQUIRED)
        );

        var rejector_list = new List<BabelInterop>(
            this.interopslist.Where(op => op.op == interop_type.INCOMPATIBLE)
        );

        foreach (BabelInitializer other in candidates)
        {
            missing.RemoveAll(bi =>
            {
                if (other.name != bi.depname)
                    return false;

                if (bi.version == null)
                    return true;
                int comp = other.version.CompareTo(bi.assembly_version);
                return comp == (int)bi.comp || comp == 0;
            });
            var rejectors = rejector_list.Where(bi =>
            {
                if (other.name != bi.depname)
                    return false;

                if (bi.version == null)
                    return true;
                int comp = other.version.CompareTo(bi.assembly_version);
                return comp == (int)bi.comp || comp == 0;
            });
            incompatible.AddRange(rejectors);
        }
        return !(missing.Any() || incompatible.Any());
    }

    public enum satisfaction_type
    {
        impossible, //incompatibilities in the graph
        unsatisfied, //missing required
        satisfied_requirements, //missing soft reqs
        satisfied_fully, //soft req's satisfied too
    };

    /// <summary>
    /// 
    /// </summary>
    /// <param name="loaded"></param>
    /// <param name="soft_missing">null if precheck fails.</param>
    /// <returns></returns>
    public satisfaction_type is_satisfied(IEnumerable<BabelInitializer> loaded, out List<BabelInterop> soft_missing)
    {
        List<BabelInterop> incompatible, missing;
        if (this.precheck(loaded, out incompatible, out missing))
        {
            soft_missing = new List<BabelInterop>(
                this.interopslist.Where(op => op.op == interop_type.SOFT)
            );
            foreach (BabelInitializer other in loaded)
            {
                soft_missing.RemoveAll(bi =>
                {
                    if (other.name != bi.depname)
                        return false;

                    if (bi.version == null)
                        return true;
                    int comp = other.version.CompareTo(this.vers);
                    return comp == (int)bi.comp || comp == 0;
                });
            }
            return soft_missing.Any() ? satisfaction_type.satisfied_requirements : satisfaction_type.satisfied_fully;
        }
        else
            soft_missing = null;

        return incompatible.Any() ? satisfaction_type.impossible : satisfaction_type.unsatisfied;
    }

    public  readonly Assembly assy;
    private readonly ModInitializer init;
    private readonly Version vers;
    private readonly List<BabelInterop> interopslist;
    private static Version defvers = new Version("1.0.0.0");
    private status_type init_status;
}
