﻿using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Babel")]

namespace Babel.Interop;

public class BabelInitializationArgs
{
    public BabelInitializationArgs(IEnumerable<Assembly> assys) => this.assemblies = new List<Assembly>(assys);
    public List<Assembly> assemblies { get; }
}

public delegate void BabelInitializationCallback(object sender, BabelInitializationArgs args);

public class BabelCallbacks
{
    public BabelCallbacks()
    {
    }

    public event BabelInitializationCallback OnInitialization;

    internal void iv_OnInitialization(IEnumerable<Assembly> assys) => this.OnInitialization?.Invoke(null, new BabelInitializationArgs(assys));
}
