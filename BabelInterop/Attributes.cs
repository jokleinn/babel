﻿using System;

namespace Babel.Interop;

public enum version_comparator
{
    AT_MOST = -1,
    EXACT = 0,
    AT_LEAST = 1
}

public enum interop_type
{
    /// <summary>
    /// Need depname to exist; load me after it.
    /// </summary>
    REQUIRED,
    /// <summary>
    /// Prefer to load me after the plugin called depname.
    /// </summary>
    SOFT,
    /// <summary>
    /// Do not load me if depname exists.
    /// </summary>
    INCOMPATIBLE
}

[AttributeUsage(AttributeTargets.Assembly, Inherited = true, AllowMultiple = false)]
public class BabelIgnore : Attribute
{
    /// <summary>
    /// Used to mark a mod to be ignored by Babel.
    /// The mod will not be loaded by Babel, but the vanilla
    /// ModLoader could still load it.
    /// </summary>
    public BabelIgnore()
    {
    }
}

[AttributeUsage(AttributeTargets.Assembly, Inherited = true, AllowMultiple = true)]
public class BabelInterop : Attribute
{
    public readonly string depname;
    public readonly interop_type op;
    public readonly string version;
    public readonly Version assembly_version;
    public readonly version_comparator comp;
    public string comp_str
    {
        get
        {
            switch (this.comp)
            {
                case version_comparator.AT_MOST:
                    {
                        return "at most";
                    }
                case version_comparator.EXACT:
                    {
                        return "exactly";
                    }
                case version_comparator.AT_LEAST:
                    {
                        return "at least";
                    }
            }
            return "";
        }
    }

    public string interop_str
    {
        get
        {
            switch (this.op)
            {
                case interop_type.REQUIRED:
                    {
                        return "requires";
                    }
                case interop_type.SOFT:
                    {
                        return "prefers to load after";
                    }
                case interop_type.INCOMPATIBLE:
                    {
                        return "is incompatible with";
                    }
            }
            return "";
        }
    }

    public string description => this.interop_str + " " + this.depname + (this.version != null ? ", version " + this.comp_str + " " + this.version : "");

    /// <summary>
    /// Used to define compatibility information for a mod.
    /// </summary>
    /// <param name="depname">The assembly name of the mod this tag applies to.</param>
    /// <param name="op">What kind of interaction we have with the mod called depname</param>
    /// <param name="version">null (all versions), or a particular version this tag is relevant for</param>
    /// <param name="comp">Allows to describe inequalities with the version info, rather than a specific version only</param>
    public BabelInterop(
        string depname,
        interop_type op = interop_type.REQUIRED,
        string version = null,
        version_comparator comp = version_comparator.AT_LEAST
    )
    {
        this.depname = depname;
        this.op = op;
        this.version = version;
        this.comp = comp;
        this.assembly_version = version == null ? null : new Version(version);
    }
}
