﻿using RestEase;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace BabelUpdater
{

    namespace GitLabData
    {
        public class Commit
        {
            public string title { get; set; }
            public string message { get; set; }
            public string id { get; set; }
        }

        public class Tag
        {
            public Commit commit { get; set; }

            public string target { get; set; }

            public string name { get; set; }
        }
    }

    [Header("User-Agent", "RestEase")]
    public interface IGitLabApi
    {
        [Get("api/v4/projects/{id}/repository/tags")]
        Task<GitLabData.Tag[]> GetTagsAsync([Path] string id);
    }

    internal static class GitLab
    {
        //note: pipeline requests give 20 pipelines back at a time
        //if they are not sorted (they should be sorted), this may be a complication
        internal const string proj_id = "40139101";

        internal static void check(Version current_version, string babeldllpath)
        {
            try
            {
                IGitLabApi api = RestClient.For<IGitLabApi>("https://gitlab.com");

                Version latest = new Version("0.0.0.0");
                GitLabData.Tag latest_tag = null;
                GitLabData.Tag[] tags = api.GetTagsAsync(proj_id).Result;
                if (tags == null)
                {
                    Console.Error.WriteLine("GitLab API gave null result");
                    return;
                }
                foreach (GitLabData.Tag t in tags)
                {
                    if (t == null)
                    {
                        Console.Error.WriteLine("GitLab API gave null tag");
                        continue;
                    }
                    if (t.name == null)
                    {
                        Console.Error.WriteLine("Nameless tag found");
                        continue;
                    }
                    if (t.commit == null)
                    {
                        Console.Error.WriteLine("Tag without commit found");
                        continue;
                    }
                    if (t.commit.title == null)
                    {
                        Console.Error.WriteLine("Untitled commit found");
                        continue;
                    }

                    Version tmp;
                    if (Version.TryParse(t.name, out tmp))
                    {
                        if (tmp > latest)
                        {
                            latest = tmp;
                            latest_tag = t;
                        }
                    }
                }
                if (latest > current_version)
                {
                    Console.WriteLine("An update is available!");
                    Console.WriteLine("Version " + latest.ToString());
                    Console.WriteLine("Commit: " + latest_tag.commit.title);

                    GitUpdater.update(latest_tag.commit.id, babeldllpath);
                }
                else
                {
                    Console.WriteLine("Babel is up to date.");
                    FileInfo babelcoredll = new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                    string[] checknames =
                    {
                        Path.Combine(babelcoredll.Directory.FullName, "plugins", "__Babel.dll"),
                        Path.Combine(babelcoredll.Directory.FullName, "plugins", "__BabelInterop.dll"),
                    };
                    foreach (string c in checknames)
                    {
                        if (File.Exists(c))
                        {
                            Console.WriteLine("Cleaning up " + c + " after last update...");
                            File.Delete(c);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
            }
        }
    }
}
