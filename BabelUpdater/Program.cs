﻿using System;

namespace BabelUpdater;

internal class Program
{
    static void Main(string[] args)
    {
        if (args.Length != 2)
        {
            return;
        }
        Version installed_version = new Version(args[0]);
        string babeldllpath = args[1];
        GitLab.check(installed_version, babeldllpath);
    }
}
