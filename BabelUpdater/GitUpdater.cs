﻿using LibGit2Sharp;
using System;
using System.IO;
using System.Linq;

namespace BabelUpdater;
internal static class GitUpdater
{
    private static void cp(string src, string dest)
    {
        var dir = new DirectoryInfo(src);
        if (!dir.Exists)
            return;

        DirectoryInfo[] dirs = dir.GetDirectories();

        Directory.CreateDirectory(dest);

        foreach (FileInfo file in dir.GetFiles())
        {
            string targetFilePath = Path.Combine(dest, file.Name);
            if (File.Exists(targetFilePath))
            {
                string priv = Path.Combine(Path.GetDirectoryName(targetFilePath), "__" + Path.GetFileName(targetFilePath));
                if (File.Exists(priv))
                    File.Delete(priv);
                Console.WriteLine("Backing up " + targetFilePath + "...");
                File.Move(targetFilePath, priv);
            }
            file.CopyTo(targetFilePath);
        }

        foreach (DirectoryInfo subDir in dirs)
        {
            string newdest = Path.Combine(dest, subDir.Name);
            cp(subDir.FullName, newdest);
        }
    }

    private static void normalize(DirectoryInfo d)
    {
        foreach (var sub in d.GetDirectories())
            normalize(sub);
        foreach (var f in d.GetFiles())
            f.Attributes = FileAttributes.Normal;
        d.Attributes = FileAttributes.Normal;
    }

    public static void update(string sha, string babeldllpath)
    {
        string safedir = Path.Combine(Path.GetTempPath(), "babel-update");
        if (Directory.Exists(safedir))
        {
            DirectoryInfo _d = new DirectoryInfo(safedir);
            normalize(_d);
            Directory.Delete(safedir, true);
        }

        Console.WriteLine("Cloning repository...");
        Repository.Clone("https://gitlab.com/jokleinn/babel.git", safedir);
        DirectoryInfo d = new DirectoryInfo(safedir);
        normalize(d);
        using (var repo = new Repository(safedir))
        {
            if (repo == null)
            {
                Console.Error.WriteLine("Cloning failed.");
                return;
            }

            Commit c;
            try
            {
                c = repo.Commits.First(x => x.Sha == sha);
            }
            catch
            {
                Console.Error.WriteLine("Failed to find commit in cloned repository.");
                return;
            }

            var b = Commands.Checkout(repo, c);
            normalize(d);
        }

        //babel-update should be full of the tag's files by now
        Console.WriteLine("Copying updated plugin files...");
        FileInfo babelcoredll = new FileInfo(Path.GetDirectoryName(babeldllpath));
        cp(Path.Combine(safedir, "prepack", "plugins"), Path.Combine(babelcoredll.Directory.FullName, "plugins"));
        d.Delete(true);
        Console.WriteLine("Update complete!");
    }
}
